/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2017 Michael Kern
**  Copyright 2017 Marc Rautenhaus
**
**  Computer Graphics and Visualization Group
**  Technische Universitaet Muenchen, Garching, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/
#include "abstractlineactor.h"

// standard library imports

// related third party imports
#include <log4cplus/loggingmacros.h>

// local application imports

namespace Met3D
{

/******************************************************************************
***                     CONSTRUCTOR / DESTRUCTOR                            ***
*******************************************************************************/

MAbstractLineActor::MAbstractLineActor()
    : MNWPMultiVarActor()
{
    beginInitialiseQtProperties();

    // Remove labels property group since it's unnecessary
    actorPropertiesSupGroup->removeSubProperty(labelPropertiesSupGroup);

    variableSettings = new VariableSettings(this);
    actorPropertiesSupGroup->addSubProperty(variableSettings->groupProp);

    testProperty = addProperty(CLICK_PROPERTY, "Test indices",
                               actorPropertiesSupGroup);

    endInitialiseQtProperties();
}


/******************************************************************************
***                            PUBLIC METHODS                               ***
*******************************************************************************/

void MAbstractLineActor::reloadShaderEffects()
{

}


const QList<MVerticalLevelType> MAbstractLineActor::supportedLevelTypes()
{
    return (QList<MVerticalLevelType>()
            << HYBRID_SIGMA_PRESSURE_3D
            << AUXILIARY_PRESSURE_3D
            << PRESSURE_LEVELS_3D
            << LOG_PRESSURE_LEVELS_3D
            << SURFACE_2D);
}


MNWPActorVariable *MAbstractLineActor::createActorVariable(
        const MSelectableDataSource &dataSource)
{
    MNWPActorVariable* newVar = new MNWPActorVariable(this);

    newVar->dataSourceID = dataSource.dataSourceID;
    newVar->levelType = dataSource.levelType;
    newVar->variableName = dataSource.variableName;

    return newVar;
}

/******************************************************************************
***                            PROTECTED METHODS                            ***
*******************************************************************************/

void MAbstractLineActor::onQtPropertyChanged(QtProperty *property)
{
    // Parent signal processing
    MNWPMultiVarActor::onQtPropertyChanged(property);

    if (suppressActorUpdates())
    {
        return;
    }

    if (property == testProperty)
    {
        QString s("Indices: ");
        foreach (int idx, variableSettings->variableIndices)
        {
            s = s.append("%1 ").arg(idx);
        }

        LOG4CPLUS_DEBUG(mlog, s.toStdString());
        return;
    }

    // Loop through every detection variable property to find changes.
    bool detectionVariableChanged = false;
    for (int i = 0; i < variableSettings->variableProperties.size(); ++i)
    {
        QtProperty *prop = variableSettings->variableProperties[i];

        if (property == prop)
        {
            variableSettings->variableIndices[i] =
                    properties->mEnum()->value(prop);

            detectionVariableChanged = true;

            emitActorChangedSignal();

            // A property has changed so we don't need to continue to check the
            // other ones.
            break;
        }
    }

    if (detectionVariableChanged)
    {
        requestData();
    }
}


void MAbstractLineActor::onDeleteActorVariable(MNWPActorVariable *var)
{
    updateVariableProperties(var);
}


void MAbstractLineActor::onAddActorVariable(MNWPActorVariable *var)
{
    Q_UNUSED(var);
    updateVariableProperties(nullptr);
}


void MAbstractLineActor::onChangeActorVariable(MNWPActorVariable *var)
{
    Q_UNUSED(var);
    updateVariableProperties(nullptr);
}


void MAbstractLineActor::updateVariableProperties(MNWPActorVariable *var)
{
    // If the actor has no detection variables, we skip the method.
    if (variableSettings->variableProperties.size() <= 0)
    {
        return;
    }

    QStringList names;

    int editedVariableIndex = -1;
    for (int i = 0; i < variables.size(); ++i)
    {
        MNWPActorVariable *variable = variables.at(i);

        if (var == nullptr || var != variable)
        {
            names << variable->variableName;
        }
        else
        {
            editedVariableIndex = i;
        }
    }

//    enableActorUpdates(false);

    foreach (QtProperty *prop, variableSettings->variableProperties)
    {
        int index = properties->mEnum()->value(prop);
        properties->mEnum()->setEnumNames(prop, names);

        if (index >= 0 && index != editedVariableIndex)
        {
            properties->mEnum()->setValue(prop, index);
        }
    }

//    enableActorUpdates(true);
}


/******************************************************************************
***                            PRIVATE METHODS                              ***
*******************************************************************************/


/******************************************************************************
***                             SETTINGS METHODS                            ***
*******************************************************************************/

MAbstractLineActor::VariableSettings::VariableSettings(
        MAbstractLineActor *actor)
{
    hostActor = actor;

    groupProp = hostActor->addProperty(GROUP_PROPERTY, "detection variables");
}


void MAbstractLineActor::VariableSettings::addVariable(QString name)
{
    if (propertyNames.contains(name))
    {
        LOG4CPLUS_WARN(mlog, "Variable property with name [" << name.toStdString()
                       << "] already exists!");
        return;
    }

    propertyNames << name;
    variableIndices << -1;
    variableProperties << hostActor->addProperty(ENUM_PROPERTY, name, groupProp);
}


void MAbstractLineActor::VariableSettings::addVariables(QStringList names)
{
    foreach (QString s, names)
    {
        addVariable(s);
    }
}


} // namespace Met3D
