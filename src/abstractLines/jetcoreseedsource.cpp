/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2017 Michael Kern
**  Copyright 2017 Marc Rautenhaus
**
**  Computer Graphics and Visualization Group
**  Technische Universitaet Muenchen, Garching, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/
#include "jetcoreseedsource.h"


namespace Met3D
{

/******************************************************************************
***                     CONSTRUCTOR / DESTRUCTOR                            ***
*******************************************************************************/

MJetcoreSeedSource::MJetcoreSeedSource()
    : MScheduledDataSource()
{
}


MJetcoreSeedSource::~MJetcoreSeedSource()
{
}


void MJetcoreSeedSource::setInputSources(MWeatherPredictionDataSource *uDataSource, MWeatherPredictionDataSource *vDataSource, MWeatherPredictionDataSource *wDataSource)
{
    setInputSource(uDataSource, 0);
    setInputSource(vDataSource, 1);
    setInputSource(wDataSource, 2);
}

/******************************************************************************
***                            PUBLIC METHODS                               ***
*******************************************************************************/

MJetcoreSeeds *MJetcoreSeedSource::produceData(MDataRequest request)
{
    assert(inputSources[0] != nullptr);
    assert(inputSources[1] != nullptr);
    assert(inputSources[2] != nullptr);

#define MSTOPWATCH_ENABLED
#ifdef MSTOPWATCH_ENABLED
    MStopwatch stopwatch;
#endif

    // Read request parameters.
    MDataRequestHelper rh(request);
    QDateTime initTime = rh.timeValue("INIT_TIME");
    QDateTime validTime = rh.timeValue("VALID_TIME");
    uint member = rh.intValue("MEMBER");
    QStringList varNames = rh.value("VARIABLES").split("/");
    float maxPressure = rh.floatValue("MAX_PRESSURE");
    float minPressure = rh.floatValue("MIN_PRESSURE");
    float minSpeed = rh.floatValue("MIN_SPEED");

    LOG4CPLUS_DEBUG(mlog, "Starting computation of jetcore seeds. "
                          "Forecast IT="
                    << initTime.toString(Qt::ISODate).toStdString()
                    << ", Date="
                    << validTime.toString(Qt::ISODate).toStdString()
                    << ", ensemble member=" << member);

    // Remove variables used by this instance
    rh.remove("MAX_PRESSURE");
    rh.remove("MIN_PRESSURE");
    rh.remove("MIN_SPEED");
    rh.remove("VARIABLES");

    // Compute seed points
    SeedComputationHelper ch;
    ch.vertices = QVector<QVector3D>();
    ch.baseRequest = rh.request();
    ch.varNames = varNames;
    ch.minSpeed = minSpeed;
    ch.minPressure = minPressure;
    ch.maxPressure = maxPressure;

    computeSeedPoints(ch);

    MJetcoreSeeds *seeds = new MJetcoreSeeds(ch.vertices, ch.values);
    seeds->setMetaData(initTime, validTime, "MET3DCOMPUTED_jetcoreSeeds",
                       member);

#ifdef MSTOPWATCH_ENABLED
    stopwatch.split();
    LOG4CPLUS_DEBUG(mlog, "Jetcore seeds computed in "
                    << stopwatch.getLastSplitTime(MStopwatch::SECONDS)
                    << " seconds.\n" << flush);
#endif

    return seeds;
}


MTask *MJetcoreSeedSource::createTaskGraph(MDataRequest request)
{
    // Create a new task.
    MTask *task = new MTask(request, this);
    MDataRequestHelper rh(request);

    const QStringList vars = rh.value("VARIABLES").split("/");

    // Remove variables used by this instance.
    rh.remove("MIN_PRESSURE");
    rh.remove("MAX_PRESSURE");
    rh.remove("MIN_SPEED");
    rh.remove("VARIABLES");

    // Get parameters from request.
    for (int i = 0; i < inputSources.size(); ++i)
    {
        rh.insert("VARIABLE", vars[i]);

        varRequests[i] = rh.request();
        task->addParent(inputSources[i]->getTaskGraph(rh.request()));
    }

    return task;
}

/******************************************************************************
***                            PROTECTED METHODS                            ***
*******************************************************************************/

void MJetcoreSeedSource::computeSeedPoints(SeedComputationHelper &ch)
{
    QVector<MStructuredGrid*> grids(ch.varNames.size(), nullptr);
    MDataRequestHelper rh(ch.baseRequest);

    bool allDataFieldsValid = true;
    for (int v = 0; v < ch.varNames.size(); ++v)
    {
        rh.insert("VARIABLE", ch.varNames[v]);

        grids[v] = inputSources[v]->getData(rh.request());
        if (grids[v] == nullptr)
        {
            allDataFieldsValid = false;
            QString emsg = QString("ERROR: Not all wind components required "
                                   "for the seed computation could be loaded. "
                                   "Please check the console output and your "
                                   "datasets. Aborting seeds computation.");
            LOG4CPLUS_ERROR(mlog, emsg.toStdString());
        }
    }

    if (allDataFieldsValid)
    {
        // Get a grid for doing calculations.
        MStructuredGrid* grid = grids[0];

        unsigned int numLons = grid->getNumLons();
        unsigned int numLats = grid->getNumLats();
        unsigned int numLevels = grid->getNumLevels();

        const double *lons = grid->getLons();
        const double *lats = grid->getLats();

        // Level indices to filter initial seed point set.
        int lowestLevel = numLevels - 1;
        int highestLevel = 0;

        // Loop through lats and lons to filter out unwanted
        // pressure levels. In order to avoid wrong levels being
        // outputted we need to check if the resulting pressure
        // at the calculated level is valid.
        for (unsigned int lat = 0; lat < numLats; ++lat)
        {
            for (unsigned int lon = 0; lon < numLons; ++lon)
            {
                // new value to check against lowestLevel
                int maxPressureLevel = grid->findClosestLevel(lat, lon, ch.maxPressure);
                // new value to check against highestLevel
                int minPressureLevel = grid->findClosestLevel(lat, lon, ch.minPressure);

                // Check if pressure values are valid.
                // Pressure values should be the same for all grids,
                // so we can just use one.
                // If values are valid, check if level values need to be updated.
                float maxP = grid->getPressure(maxPressureLevel, lat, lon);
                if (maxP > -1000
                        && maxPressureLevel > highestLevel)
                {
                    highestLevel = maxPressureLevel;
                }

                float minP = grid->getPressure(minPressureLevel, lat, lon);
                if (minP > -1000
                        && minPressureLevel < lowestLevel)
                {
                    lowestLevel = minPressureLevel;
                }
            }
        }

        QVector<float> windMagnitudeField =
                getWindMagnitudeField(grids, highestLevel, lowestLevel);

        // Temp vector to store the vertices and their values
        QVector<SeedVertex> seedPoints;

        // Loop through levels, lats and lons (k, j, i)
        // from lowest pressure to highest pressure.
        for (int k = lowestLevel + 1, magFldLevel = 1; k < highestLevel;
             ++k, ++magFldLevel)
        {
            for (unsigned int j = 1; j < numLats - 1; ++j)
            {
                for (unsigned int i = 1; i < numLons - 1; ++i)
                {
                    int index = (magFldLevel * numLons * numLats) + (numLons * j) + i;
                    float windMagnitude = windMagnitudeField[index];

                    // Check if windMagnitude is valid and over threshold
                    if (windMagnitude != -1 && windMagnitude > ch.minSpeed)
                    {
                        // Point is a seed point when it is a local maximum.
                        // If all points around the current point have a smaller windMagnitude
                        // than the current point.
                        bool isSeedPoint = true;
                        for (int lvl = -1; lvl <= 1; ++lvl)
                        {
                            for (int lat = -1; lat <= 1; ++lat)
                            {
                                for (int lon = -1; lon <= 1; ++lon)
                                {
                                    // we can skip the current point because the check
                                    // against the threshold was already done.
                                    if (lvl == 0 && lat == 0 && lon == 0)
                                        continue;

                                    int idx = ((magFldLevel + lvl) * numLons * numLats) + (numLons * (j + lat)) + i + lon;
                                    float mag = windMagnitudeField[idx];
                                    if (windMagnitude < mag)
                                    {
                                        isSeedPoint = false;
                                        break;
                                    }
                                }

                                if (!isSeedPoint)
                                    break;
                            }

                            if (!isSeedPoint)
                                break;
                        }

                        if (!isSeedPoint)
                            continue;

                        // If current point is a seed point:
                        // Get values for seed points...
                        double lon = lons[i];
                        double lat = lats[j];
                        float pressure = grid->getPressure(
                                    k, j, i);

                        SeedVertex vertex;
                        // ...and add current point to the vertices and values.
                        vertex.position = QVector3D(lon, lat, pressure);
                        vertex.windMagnitude = windMagnitude;

                        seedPoints << vertex;
                    }
                }
            }
        }

        sortSeeds(seedPoints);

        foreach (SeedVertex vertex, seedPoints)
        {
            ch.vertices << vertex.position;
            ch.values << vertex.windMagnitude;
        }
    }

    // Release wind data grids
    for (int v = 0; v < ch.varNames.size(); ++v)
    {
        if (grids[v])
        {
            inputSources[v]->releaseData(grids[v]);
        }
    }
}


const QStringList MJetcoreSeedSource::locallyRequiredKeys()
{
    return (QStringList() << "MEMBER" << "VARIABLES" << "MIN_SPEED"
            << "MIN_PRESSURE" << "MAX_PRESSURE");
}

/******************************************************************************
***                            PRIVATE METHODS                              ***
*******************************************************************************/

void MJetcoreSeedSource::setInputSource(MWeatherPredictionDataSource *dataSource,
                                        int index)
{
    inputSources[index] = dataSource;
    registerInputSource(inputSources[index]);
    enablePassThrough(inputSources[index]);
}


QVector<float> MJetcoreSeedSource::getWindMagnitudeField(
        QVector<MStructuredGrid*> grids,
        int highestLevel, int lowestLevel)
{
    MStructuredGrid *uGrid = grids[0];
    MStructuredGrid *vGrid = grids[1];
    MStructuredGrid *wGrid = grids[2];

    unsigned int numLons = uGrid->getNumLons();
    unsigned int numLats = uGrid->getNumLats();

    QVector<float> windMagnitudeField;
    for (int k = lowestLevel; k <= highestLevel; ++k)
    {
        for (unsigned int j = 0; j < numLats; ++j)
        {
            for (unsigned int i = 0; i < numLons; ++i)
            {
                float u = uGrid->getValue(k, j, i);
                float v = vGrid->getValue(k, j, i);
                float w = wGrid->getValue(k, j, i);

                bool isInvalid = u < -1000 || v < -1000 || w < -1000;

                float windMagnitude = isInvalid ? -1 : QVector3D(u, v, w).length();
                windMagnitudeField << windMagnitude;
            }
        }
    }

    return windMagnitudeField;
}


void MJetcoreSeedSource::sortSeeds(QVector<SeedVertex> &points)
{
    quickSort(points, 0, points.size() - 1);
}


void MJetcoreSeedSource::quickSort(QVector<SeedVertex> &points, int lo, int hi)
{
    if (lo >= hi || lo < 0)
        return;

    // Partition array and get the pivot index
    int p = partition(points, lo, hi);

    // Sort the two partitions
    quickSort(points, lo, p - 1);
    quickSort(points, p + 1, hi);
}


int MJetcoreSeedSource::partition(QVector<SeedVertex> &points, int lo, int hi)
{
    // Choose last element as pivot
    SeedVertex pivot = points[hi];

    // Temporary pivot index
    int i = lo - 1;

    for (int j = lo; j < hi; ++j)
    {
        if (points[j].windMagnitude > pivot.windMagnitude)
        {
            i++;
            SeedVertex temp = points[i];
            points[i] = points[j];
            points[j] = temp;
        }
    }

    ++i;
    SeedVertex temp = points[i];
    points[i] = points[hi];
    points[hi] = temp;
    return i;
}

} // namespace Met3D
