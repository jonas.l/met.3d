/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2016 ...
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/
#ifndef MJETCORESEEDS_H
#define MJETCORESEEDS_H

// standard library imports

// related third party imports

// local application imports
#include "data/abstractdataitem.h"
#include "gxfw/gl/typedvertexbuffer.h"

namespace Met3D
{

class MJetcoreSeeds : public MAbstractDataItem,
        public MWeatherPredictionMetaData
{
public:
    explicit MJetcoreSeeds(const QVector<QVector3D> vertices,
                           const QVector<float> values);
    ~MJetcoreSeeds();

public:
    GL::MVertexBuffer *getVertexBuffer(QGLWidget *currentGLContext = 0);
    GL::MVertexBuffer *getValuesVertexBuffer(QGLWidget *currentGLContext = 0);

    void releaseVertexBuffer();
    void releaseValuesVertexBuffer();

    const int getNumPoints() { return vertices.size(); }
    const QVector<QVector3D> &getVertices() { return vertices; }
    const QVector<float> &getValues() { return values; }

    // MAbstractDataItem interface
    unsigned int getMemorySize_kb();

private:
    QVector<QVector3D> vertices;
    QVector<float> values;
};

} // namespace Met3D

#endif // MJETCORESEEDS_H
