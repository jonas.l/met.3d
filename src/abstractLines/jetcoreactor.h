/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2016 ...
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/
#ifndef MJETCOREACTOR_H
#define MJETCOREACTOR_H

// standard library imports

// related third party imports

// local application imports
#include "abstractlineactor.h"
#include "jetcoreseedsource.h"

namespace Met3D
{

class MJetcoreActor : public MAbstractLineActor
{
    Q_OBJECT

public:
    MJetcoreActor();
//    ~MJetcoreActor() override;

    static QString staticActorType() { return "Jetcores";}

    void setDataSource(MJetcoreSeedSource *ds);

    // MActor interface
    QString getSettingsID() override { return "JetcoreDetectionActor"; }
    void saveConfiguration(QSettings *settings) override;
    void loadConfiguration(QSettings *settings) override;
    void reloadShaderEffects() override;

public slots:
    void asynchronousDataAvailable(MDataRequest request);

protected:
    void requestData() override;

    // MActor interface
    void initializeActorResources() override;
    void renderToCurrentContext(MSceneViewGLWidget *sceneView) override;

    // MNWPMultiVarActor interface
    void dataFieldChangedEvent() override;

private:
    std::shared_ptr<GL::MShaderEffect> geometryShader;
    std::shared_ptr<GL::MShaderEffect> positionSphereShader;
    std::shared_ptr<GL::MShaderEffect> positionSphereShadowShader;

    MJetcoreSeedSource *seedSource;
    MJetcoreSeeds *seeds;

    GL::MVertexBuffer *vertexBuffer;
    GL::MVertexBuffer *valuesBuffer;
};


class MJetcoreActorFactory : public MAbstractActorFactory
{
public:
    MJetcoreActorFactory() : MAbstractActorFactory() {}

protected:
    MActor *createInstance() override
    {
        return new MJetcoreActor();
    }
};

} // namespace Met3D

#endif // MJETCOREACTOR_H
