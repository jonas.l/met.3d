/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2017 Michael Kern
**  Copyright 2017 Marc Rautenhaus
**
**  Computer Graphics and Visualization Group
**  Technische Universitaet Muenchen, Garching, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/
#include "jetcores.h"

namespace Met3D
{

/******************************************************************************
***                     CONSTRUCTOR / DESTRUCTOR                            ***
*******************************************************************************/

MJetcoreSeeds::MJetcoreSeeds(const QVector<QVector3D> vertices,
                             const QVector<float> values)
    : MWeatherPredictionMetaData(),
      vertices(vertices),
      values(values)
{
}


MJetcoreSeeds::~MJetcoreSeeds()
{
    MGLResourcesManager::getInstance()->releaseAllGPUItemReferences(getID());
}

/******************************************************************************
***                            PUBLIC METHODS                               ***
*******************************************************************************/

GL::MVertexBuffer *MJetcoreSeeds::getVertexBuffer(QGLWidget *currentGLContext)
{
    MGLResourcesManager *glRM = MGLResourcesManager::getInstance();

    // Check if a texture with this item's data already exists in GPU memory.
    GL::MVertexBuffer *vb = static_cast<GL::MVertexBuffer*>(
                glRM->getGPUItem(getID() + "_seeds"));
    if (vb) return vb;

    // No texture with this item's data exists create a new one.
    GL::MVector3DVertexBuffer *newVB = new GL::MVector3DVertexBuffer(
                getID() + "_seeds", vertices.size());

    if (glRM->tryStoreGPUItem(newVB))
    {
        newVB->upload(vertices, currentGLContext);
    }
    else
    {
        delete newVB;
    }

    return static_cast<GL::MVertexBuffer*>(glRM->getGPUItem(getID() + "_seeds"));
}


GL::MVertexBuffer *MJetcoreSeeds::getValuesVertexBuffer(
        QGLWidget *currentGLContext)
{
    MGLResourcesManager *glRM = MGLResourcesManager::getInstance();

    // Check if a texture with this item's data already exists in GPU memory.
    GL::MVertexBuffer *vb = static_cast<GL::MVertexBuffer*>(
                glRM->getGPUItem(getID() + "_seedValues"));
    if (vb) return vb;

    // No texture with this item's data exists create a new one.
    GL::MFloatVertexBuffer *newVB = new GL::MFloatVertexBuffer(
                getID() + "_seedValues", values.size());

    if (glRM->tryStoreGPUItem(newVB))
    {
        newVB->upload(values, currentGLContext);
    }
    else
    {
        delete newVB;
    }

    return static_cast<GL::MVertexBuffer*>(
                glRM->getGPUItem(getID() + "_seedValues"));
}


void MJetcoreSeeds::releaseVertexBuffer()
{
    MGLResourcesManager::getInstance()->releaseGPUItem(getID() + "_seeds");
}


void MJetcoreSeeds::releaseValuesVertexBuffer()
{
    MGLResourcesManager::getInstance()->releaseGPUItem(getID() + "_seedValues");
}


unsigned int MJetcoreSeeds::getMemorySize_kb()
{
    return (sizeof(MJetcoreSeeds)
            + vertices.size() * sizeof(QVector3D)
            + values.size() * sizeof(float));
}

/******************************************************************************
***                            PROTECTED METHODS                            ***
*******************************************************************************/

/******************************************************************************
***                            PRIVATE METHODS                              ***
*******************************************************************************/

} // namespace Met3D
