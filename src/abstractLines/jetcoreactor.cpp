/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2017 Michael Kern
**  Copyright 2017 Marc Rautenhaus
**
**  Computer Graphics and Visualization Group
**  Technische Universitaet Muenchen, Garching, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/
#include "jetcoreactor.h"

// standard library imports

// related third party imports
#include <log4cplus/loggingmacros.h>

// local application imports

namespace Met3D
{

/******************************************************************************
***                     CONSTRUCTOR / DESTRUCTOR                            ***
*******************************************************************************/

MJetcoreActor::MJetcoreActor()
    : MAbstractLineActor(),
      seedSource(nullptr),
      seeds(nullptr)
{
    setActorType(staticActorType());
    setName(getActorType());

    beginInitialiseQtProperties();

    QStringList variableNames;
    variableNames << "u-component of wind"
            << "v-component of wind"
            << "vertical wind speed";

    variableSettings->addVariables(variableNames);

    endInitialiseQtProperties();
}


void MJetcoreActor::setDataSource(MJetcoreSeedSource *ds)
{
    if (seedSource != nullptr)
    {
        disconnect(seedSource, SIGNAL(dataRequestCompleted(MDataRequest)),
                   this, SLOT(asynchronousDataAvailable(MDataRequest)));
    }

    seedSource = ds;
    if (seedSource != nullptr)
    {
        connect(seedSource, SIGNAL(dataRequestCompleted(MDataRequest)),
                this, SLOT(asynchronousDataAvailable(MDataRequest)));
    }
}

/******************************************************************************
***                            PUBLIC METHODS                               ***
*******************************************************************************/

void MJetcoreActor::saveConfiguration(QSettings *settings)
{
    MAbstractLineActor::saveConfiguration(settings);
}


void MJetcoreActor::loadConfiguration(QSettings *settings)
{
    MAbstractLineActor::loadConfiguration(settings);
}


void MJetcoreActor::reloadShaderEffects()
{
    LOG4CPLUS_DEBUG(mlog, "loading shader programs.");

    beginCompileShaders(3);

    compileShadersFromFileWithProgressDialog(
                geometryShader,
                "src/glsl/jetcore_tubes.fx.glsl");
    compileShadersFromFileWithProgressDialog(
                positionSphereShader,
                "src/glsl/trajectory_positions.fx.glsl");
    compileShadersFromFileWithProgressDialog(
                positionSphereShadowShader,
                "src/glsl/trajectory_positions_shadow.fx.glsl");

    endCompileShaders();
}


void MJetcoreActor::asynchronousDataAvailable(MDataRequest request)
{
    LOG4CPLUS_DEBUG(mlog, "seeds available");
    if (seeds)
    {
        seeds->releaseVertexBuffer();
        seeds->releaseValuesVertexBuffer();

        seedSource->releaseData(seeds);
    }
    seeds = seedSource->getData(request);

    vertexBuffer = seeds->getVertexBuffer();
    valuesBuffer = seeds->getValuesVertexBuffer();

    emitActorChangedSignal();
}

/******************************************************************************
***                            PROTECTED METHODS                            ***
*******************************************************************************/

void MJetcoreActor::requestData()
{
    if (getViews().empty())
    {
        return;
    }

    if (variableSettings->variableIndices[0] == variableSettings->variableIndices[1]
            || variableSettings->variableIndices[0] == variableSettings->variableIndices[2]
            || variableSettings->variableIndices[1] == variableSettings->variableIndices[2])
    {
        return;
    }

    LOG4CPLUS_DEBUG(mlog, "Start data request!");

    if (seedSource == nullptr)
    {
        MSystemManagerAndControl *sysMC = MSystemManagerAndControl::getInstance();
        MAbstractScheduler *scheduler = sysMC->getScheduler("MultiThread");
        MAbstractMemoryManager *memoryManager = sysMC->getMemoryManager("NWP");

        seedSource = new MJetcoreSeedSource();
        seedSource->setScheduler(scheduler);
        seedSource->setMemoryManager(memoryManager);

        setDataSource(seedSource);
        sysMC->registerDataSource("jetcoreseedsource", seedSource);
    }

    // Set detection variables in input source.
    auto uVar = dynamic_cast<MNWPActorVariable*>(
                variables.at(variableSettings->variableIndices[0]));
    auto vVar = dynamic_cast<MNWPActorVariable*>(
                variables.at(variableSettings->variableIndices[1]));
    auto wVar = dynamic_cast<MNWPActorVariable*>(
                variables.at(variableSettings->variableIndices[2]));

    seedSource->setInputSources(uVar->dataSource, vVar->dataSource,
                                wVar->dataSource);

    // Build request
    MDataRequestHelper rh;

    rh.insert("INIT_TIME", uVar->getPropertyTime(uVar->initTimeProperty));
    rh.insert("VALID_TIME", uVar->getPropertyTime(uVar->validTimeProperty));
    rh.insert("LEVELTYPE", uVar->levelType);
    rh.insert("MEMBER", uVar->getEnsembleMember());

    rh.insert("MIN_PRESSURE", QString("%1").arg(190.));
    rh.insert("MAX_PRESSURE", QString("%1").arg(350.));
    rh.insert("MIN_SPEED", QString("%1").arg(40.));

    rh.insert("VARIABLES", uVar->variableName + "/" + vVar->variableName + "/"
              + wVar->variableName);

    seedSource->requestData(rh.request());

    LOG4CPLUS_DEBUG(mlog, "Data requested!");
}


void MJetcoreActor::initializeActorResources()
{
    bool loadShaders = false;
    MGLResourcesManager *glRM = MGLResourcesManager::getInstance();

    loadShaders |= glRM->generateEffectProgram("simple_geometry",
                                               geometryShader);
    loadShaders |= glRM->generateEffectProgram("trajectory_spheres",
                                               positionSphereShader);
    loadShaders |= glRM->generateEffectProgram("trajectory_spheresshadow",
                                               positionSphereShadowShader);

    if (loadShaders) reloadShaderEffects();
}


void MJetcoreActor::renderToCurrentContext(MSceneViewGLWidget *sceneView)
{
    if (seeds == nullptr)
    {
        return;
    }

    positionSphereShader->bindProgram("Normal");

    positionSphereShader->setUniformValue(
                "mvpMatrix",
                *(sceneView->getModelViewProjectionMatrix())); CHECK_GL_ERROR;
    positionSphereShader->setUniformValue(
                "pToWorldZParams",
                sceneView->pressureToWorldZParameters()); CHECK_GL_ERROR;
    positionSphereShader->setUniformValue(
                "lightDirection",
                sceneView->getLightDirection()); CHECK_GL_ERROR;
    positionSphereShader->setUniformValue(
                "cameraPosition",
                sceneView->getCamera()->getOrigin()); CHECK_GL_ERROR;
    positionSphereShader->setUniformValue(
                "cameraUpDir",
                sceneView->getCamera()->getYAxis()); CHECK_GL_ERROR;
    positionSphereShader->setUniformValue(
                "radius",
                0.5f); CHECK_GL_ERROR;
    positionSphereShader->setUniformValue(
                "constColour",
                QColor(0, 255, 0, 255));
    positionSphereShader->setUniformValue(
                "scaleRadius",
                GLboolean(false)); CHECK_GL_ERROR;

    positionSphereShader->setUniformValue(
                "useTransferFunction",
                false); CHECK_GL_ERROR;

    positionSphereShader->setUniformValue(
                "renderAuxData",
                GLboolean(false)); CHECK_GL_ERROR;

    // DRAW COMMANDS

    // Bind vertex buffer object.
    vertexBuffer->attachToVertexAttribute(GLint(0));
    valuesBuffer->attachToVertexAttribute(GLint(2));

    glPolygonMode(GL_FRONT_AND_BACK,
                  renderAsWireFrame ? GL_LINE : GL_FILL); CHECK_GL_ERROR;
    glLineWidth(1);

    glDrawArrays(GL_POINTS, 0, seeds->getNumPoints()); CHECK_GL_ERROR;

    // Shadows

    positionSphereShadowShader->bind();

    positionSphereShadowShader->setUniformValue(
            "mvpMatrix",
            *(sceneView->getModelViewProjectionMatrix())); CHECK_GL_ERROR;
    positionSphereShadowShader->setUniformValue(
            "pToWorldZParams",
            sceneView->pressureToWorldZParameters()); CHECK_GL_ERROR;
    positionSphereShadowShader->setUniformValue(
            "lightDirection",
            sceneView->getLightDirection()); CHECK_GL_ERROR;
    positionSphereShadowShader->setUniformValue(
            "cameraPosition",
            sceneView->getCamera()->getOrigin()); CHECK_GL_ERROR;
    positionSphereShadowShader->setUniformValue(
            "radius",
            0.5f); CHECK_GL_ERROR;
    positionSphereShadowShader->setUniformValue(
            "scaleRadius",
            GLboolean(false)); CHECK_GL_ERROR;

    positionSphereShadowShader->setUniformValue(
            "useTransferFunction",
            GLboolean(false)); CHECK_GL_ERROR;

    positionSphereShadowShader->setUniformValue(
            "constColour", QColor(20, 20, 20, 155)); CHECK_GL_ERROR;

    // DRAW COMMANDS

    glDrawArrays(GL_POINTS, 0, seeds->getNumPoints()); CHECK_GL_ERROR;

    // Unbind VBO.
    glBindBuffer(GL_ARRAY_BUFFER, 0);
}


void MJetcoreActor::dataFieldChangedEvent()
{
    if (variables.size() >= 3)
    {
        requestData();
    }
}

/******************************************************************************
***                            PRIVATE METHODS                              ***
*******************************************************************************/


} // namespace Met3D
