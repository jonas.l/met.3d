/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2016 ...
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/
#ifndef MABSTRACTLINEACTOR_H
#define MABSTRACTLINEACTOR_H

// standard library imports

// related third party imports

// local application imports
#include "gxfw/nwpmultivaractor.h"
#include "gxfw/nwpactorvariable.h"

namespace Met3D
{

class MAbstractLineActor : public MNWPMultiVarActor
{
    Q_OBJECT

public:
    MAbstractLineActor();

    // MActor interface
    void reloadShaderEffects() override;

    // MNWPMultiVarActor interface
    const QList<MVerticalLevelType> supportedLevelTypes() override;
    MNWPActorVariable *createActorVariable(
            const MSelectableDataSource &dataSource) override;

protected:
    virtual void requestData() = 0;

    // MActor interface
    void renderToCurrentContext(MSceneViewGLWidget *sceneView) = 0;
    void onQtPropertyChanged(QtProperty *property);

    // MNWPMultiVarActor interface
    void onDeleteActorVariable(MNWPActorVariable *var) override;
    void onAddActorVariable(MNWPActorVariable *var) override;
    void onChangeActorVariable(MNWPActorVariable *var) override;

    void updateVariableProperties(MNWPActorVariable *var);

    struct VariableSettings
    {
        explicit VariableSettings(MAbstractLineActor* actor);

        MActor *hostActor;

        QtProperty *groupProp;

        QStringList propertyNames;
        QVector<int> variableIndices;
        QVector<QtProperty*> variableProperties;

        /**
          Adds an enum property with @p name to the variable settings.
        */
        void addVariable(QString name);

        /**
          Adds variables with @p names to the variable settings as enum properties.
        */
        void addVariables(QStringList names);
    };
    VariableSettings *variableSettings;

private:

    QtProperty *testProperty;
};

} // namespace Met3D

#endif // MABSTRACTLINEACTOR_H
