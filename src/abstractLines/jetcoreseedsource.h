/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2016 ...
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/
#ifndef MJETCORESEEDSOURCE_H
#define MJETCORESEEDSOURCE_H

// standard library imports

// related third party imports
#include <netcdf>

// local application imports
#include "data/abstractdatacomputation.h"
#include "data/weatherpredictiondatasource.h"
#include "jetcores.h"

namespace Met3D
{

class MJetcoreSeedSource : public MScheduledDataSource
{
public:
    MJetcoreSeedSource();
    ~MJetcoreSeedSource() override;

public:
    void setInputSources(MWeatherPredictionDataSource* uDataSource,
                         MWeatherPredictionDataSource* vDataSource,
                         MWeatherPredictionDataSource* wDataSource);

    // MAbstractDataSource interface
    MJetcoreSeeds *getData(MDataRequest request)
    { return static_cast<MJetcoreSeeds*>(MScheduledDataSource::getData(request)); }

    // MMemoryManagedDataSource interface
    MJetcoreSeeds *produceData(MDataRequest request) override;

    // MScheduledDataSource interface
    MTask *createTaskGraph(MDataRequest request) override;
protected:
    struct SeedComputationHelper
    {
        QVector<QVector3D> vertices;
        QVector<float> values;
        QStringList varNames;

        MDataRequest baseRequest;

        float minSpeed;
        float minPressure;
        float maxPressure;
    };

    struct SeedVertex
    {
        QVector3D position;
        float windMagnitude;
    };

    void computeSeedPoints(SeedComputationHelper &ch);

    // MMemoryManagedDataSource interface
    const QStringList locallyRequiredKeys() override;

private:
    void setInputSource(MWeatherPredictionDataSource* dataSource, int index);

    QVector<float> getWindMagnitudeField(
            QVector<MStructuredGrid*> grids,
            int highestLevel, int lowestLevel = 0);

    // Quicksort for sorting the seed points in descending
    // wind magnitude order.
    void sortSeeds(QVector<SeedVertex> &points);
    void quickSort(QVector<SeedVertex> &points, int lo, int hi);
    int partition(QVector<SeedVertex> &points, int lo, int hi);

    std::array<MWeatherPredictionDataSource*, 3> inputSources;
    std::array<QString, 3> varRequests;
};

} // namespace Met3D

#endif // MJETCORESEEDSOURCE_H
